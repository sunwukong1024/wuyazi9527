//
//  GCENetWorking.h
//  GCEye
//
//  Created by PSY on 2018/3/16.
//  Copyright © 2018年 Gazepm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"


#define PAGE @"page"
#define FETCHSIZE @"fetchSize"
#define RESRESULT responseObject[@"result"]
#define RESTOTALPAGE [responseObject[@"result"][@"totalPages"] integerValue]
#define RESOBJ responseObject[@"result"][@"content"]
#define RESMESSAGE responseObject[@"message"]
#define RESSTATUSTOKENERROR [responseObject[@"status"] isEqualToString:@"10"]
#define SUCCESSREQUSET [responseObject[@"status"] isEqualToString:@"0"] && [responseObject[@"message"] isEqualToString:@"success"]
@interface GCENetWorking : NSObject
+ (void)getURL:(NSString *)URL parameters:(id)parameters success:(void (^)(id responseObject))success failure:(void (^)(NSError * error))failure;
+ (void)postURL:(NSString *)URL parameters:(id)parameters success:(void (^)(id responseObject))success failure:(void (^)(NSError * error))failure;
+ (void)deleteURL:(NSString *)URL parameters:(id)parameters success:(void (^)(id responseObject))success failure:(void (^)(NSError * error))failure;
+ (void)putURL:(NSString *)URL parameters:(id)parameters success:(void (^)(id responseObject))success failure:(void (^)(NSError * error))failure;
+ (BOOL)NetworkStatusTo;
@end
