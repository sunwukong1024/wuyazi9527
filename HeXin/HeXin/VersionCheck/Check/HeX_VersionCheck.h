//
//  HeX_VersionCheck.h
//  HeXin
//
//  Created by xiaobaicai on 2020/3/9.
//  Copyright © 2020 zdb. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

#define CheckVersionURL @"http://mock-api.com/EgdQZXnM.mock/api/sysConfig/versionId"
#define NewVersionURL @"http://mock-api.com/EgdQZXnM.mock/api/sysConfig/appDownloadUrl"

@interface HeX_VersionCheck : NSObject
+ (void)checkVersion;
@end

NS_ASSUME_NONNULL_END
