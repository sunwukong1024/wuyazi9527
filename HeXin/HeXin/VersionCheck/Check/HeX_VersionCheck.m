//
//  HeX_VersionCheck.m
//  HeXin
//
//  Created by xiaobaicai on 2020/3/9.
//  Copyright © 2020 zdb. All rights reserved.
//

#import "HeX_VersionCheck.h"
#import "GCENetWorking.h"

@interface HeX_VersionCheck ()

@end

@implementation HeX_VersionCheck

+ (void)checkVersion{
    [GCENetWorking getURL:CheckVersionURL parameters:nil success:^(id responseObject) {
        //1.获取当前版本号
        NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
        
        NSString * currentAppVersion = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
        NSLog(@"当前版本%@",currentAppVersion);
        
        NSLog(@"检查版本请求成功%@",responseObject);
        if (responseObject) {
            NSDictionary * resultDic = responseObject;
            NSDictionary * dataDic = resultDic[@"data"];
            NSString *newVersion = dataDic[@"content"];
            int convert = [self convertVersion:newVersion andv2:currentAppVersion];
            if (convert == 0) {
                return;
            }else if (convert == 1){
                [self newVersionNeedUpdate];
            }else{
                return;
            }
        }
    } failure:^(NSError *error) {
        NSLog(@"检查版本请求失败%@",error);
        [self alertErrorOfNetwork];
    }];
    
}

+ (void)newVersionNeedUpdate{
    
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"温馨提示" message:@"有新的版本，是否需要更新？" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * actionCacel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    UIAlertAction * actionEnSure = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self getNewUrlOfNewVersion];
    }];
    [alert addAction:actionCacel];
    [alert addAction:actionEnSure];
        
    UIViewController * currentVC = [self currentViewController];
    [currentVC presentViewController:alert animated:YES completion:nil];
}

+ (void)getNewUrlOfNewVersion{
    [GCENetWorking getURL:NewVersionURL parameters:nil success:^(id responseObject) {
        NSLog(@"获取版本地址请求成功%@",responseObject);
        if (responseObject) {
            NSDictionary * resultDic = responseObject;
            NSDictionary * dataDic = resultDic[@"data"];
            NSString * content = dataDic[@"content"];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:content]];
        }else{
            [self alertNoneOfNewVersion];
        }
    } failure:^(NSError *error) {
        NSLog(@"获取版本地址请求失败%@",error);
        [self alertNoneOfNewVersion];
    }];
}

+ (void)alertNoneOfNewVersion{
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"错误提示" message:@"未获取到新版本" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * actionEnSure = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [alert addAction:actionEnSure];
    UIViewController * currentVC = [self currentViewController];
    [currentVC presentViewController:alert animated:YES completion:nil];
}

+ (void)alertErrorOfNetwork{
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"错误提示" message:@"网络错误，请检查网络后重试" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * actionEnSure = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [alert addAction:actionEnSure];
    UIViewController * currentVC = [self currentViewController];
    [currentVC presentViewController:alert animated:YES completion:nil];
}
     
/**
 比较版本号
 1.1 = 1.1.0
 1.0.0.1 = 1...1
 1.2 > 1.1.3
 2 > 1.9
 2.0.0.10 < 2.0.1
 @param v1 版本1
 @param v2 版本2
 @return 返回0:相等 1:v1>v2 -1:v1<v2
 */
+ (int)convertVersion:(NSString *)v1 andv2:(NSString *)v2{

    NSString *v1_n = [[v1 componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789."] invertedSet]] componentsJoinedByString:@""];
    NSString *v2_n = [[v2 componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789."] invertedSet]] componentsJoinedByString:@""];
    NSArray *v1_arr = [v1_n componentsSeparatedByString:@"."];
    NSArray *v2_arr = [v2_n componentsSeparatedByString:@"."];
    NSInteger count = MAX(v1_arr.count, v2_arr.count);
    for (NSInteger i = 0; i < count; i++) {
        
        NSInteger v1_i = 0;
        NSInteger v2_i = 0;
        
        if (v1_arr.count > i) {
            v1_i = [v1_arr[i] integerValue];
        }
        if (v2_arr.count > i) {
            v2_i = [v2_arr[i] integerValue];
        }
        
        // 按顺序比较大小
        if (v1_i != v2_i) {
            return v1_i>v2_i?1:-1;
        }
    }
    return 0;
}

+ (UIViewController*)currentViewController {
 
    UIViewController* vc = [UIApplication sharedApplication].keyWindow.rootViewController;
 
    while (1) {
    
        if ([vc isKindOfClass:[UITabBarController class]]) {
            vc = ((UITabBarController*)vc).selectedViewController;
        }
        if ([vc isKindOfClass:[UINavigationController class]]) {
            vc = ((UINavigationController*)vc).visibleViewController;
        }
        if (vc.presentedViewController) {
            vc = vc.presentedViewController;
        }else{
            break;
        }
    
    }
 
    return vc;
}
     


@end
