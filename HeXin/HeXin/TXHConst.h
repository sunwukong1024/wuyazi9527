//
//  TXHConst.h
//  CarHouseRemit
//
//  Created by Dreamer on 2019/3/27.
//  Copyright © 2019 Dreamer. All rights reserved.
//

#ifndef TXHConst_h
#define TXHConst_h

#define  kStatusBarHeight  [UIApplication sharedApplication].statusBarFrame.size.height //20.0f or 44.f in X
#define  Is_Device_iPhoneX  (kStatusBarHeight != 20)
#define  kUnSafeBottomHeight  (Is_Device_iPhoneX ? 34.f : 0.f)
#define  kTabbarHeight  (kUnSafeBottomHeight + 49.f)    //49.f or 83.f in X
#define  kNavBarHeight  44.0f
#define  kNavStatusBarHeight  (kStatusBarHeight + kNavBarHeight)

#define kScreenWidth   CGRectGetWidth([[UIScreen mainScreen] bounds])  //获取屏幕宽度
#define kScreenHeight  CGRectGetHeight([[UIScreen mainScreen] bounds]) //获取屏幕高度

/* 屏幕适配系数(以iphone-plus为基准) */
#define KScreenWidthRatio (kScreenWidth / 375.0)
#define KAdaptedScreen(length) (ceilf((length) * KScreenWidthRatio))

#define RGBA(r,g,b,a) [UIColor colorWithRed:r/255.0f green:g/255.0f blue:b/255.0f alpha:a]

#define Hex(rgbValue) ([UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0])


#define kISiPhone (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define kScreenMaxLength (MAX(kScreenWidth, kScreenHeight))
#define kScreenMinLength (MIN(kScreenWidth, kScreenHeight))
#define kISiPhone5 (kISiPhone && kScreenMaxLength == 568.0)
#define kISiPhone6 (kISiPhone && kScreenMaxLength == 667.0)
#define kISiPhone6P (kISiPhone && kScreenMaxLength == 736.0)
#define kISiPhoneX (kISiPhone && kScreenMaxLength == 812.0)
#define kISiPhoneXr (kISiPhone && kScreenMaxLength == 896.0)
#define kISiPhoneXX (kISiPhone && kScreenMaxLength > 811.0)

#endif /* TXHConst_h */
