//
//  TXHUserHelper.m
//  CarHouseRemit
//
//  Created by Dreamer on 2019/4/1.
//  Copyright © 2019 Dreamer. All rights reserved.
//

#import "TXHUserHelper.h"

#define kUserModel @"UserModel"
#define kFirstOpen @"FirstOpen"
#define kHasShowGuide @"HasShowGuide"

#define kProvince @"Province"
#define kCity @"City"

#define kVersion @"Version"

@implementation TXHUserHelper

+ (BOOL)isFirstOpen{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *str = [userDefaults objectForKey:kFirstOpen];
    if (str) {
        return NO;
    } else {
        NSUserDefaults *userDefalults2 = [NSUserDefaults standardUserDefaults];
        [userDefalults2 setObject:@"YES" forKey:kFirstOpen];
        [userDefalults2 synchronize];
        return YES;
    }
}

@end
