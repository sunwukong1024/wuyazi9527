//
//  ViewController.m
//  HeXin
//
//  Created by Dreamer on 2018/9/15.
//  Copyright © 2018年 zdb. All rights reserved.
//

#import "ViewController.h"

#import <WebKit/WebKit.h>

#import "WKWebViewJavascriptBridge.h"

#import "TXHUserHelper.h"

#import "DHGuidePageHUD.h"

#import "TXHConst.h"

@interface ViewController ()<WKNavigationDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property (nonatomic, strong) WKWebView *webView;

@property (nonatomic, strong) WKWebViewConfiguration *webViewConfig;

@property (nonatomic, strong) WKWebViewJavascriptBridge* bridge;

@property (nonatomic, copy) WVJBResponseCallback responseCallback;

@property (nonatomic, assign) BOOL isFirst;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.isFirst = YES;
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [WKWebViewJavascriptBridge enableLogging];
    self.bridge = [WKWebViewJavascriptBridge bridgeForWebView:self.webView];
    [self.bridge setWebViewDelegate:self];

    [self.bridge registerHandler:@"getPhoto" handler:^(id data, WVJBResponseCallback responseCallback) {
        if ([data isEqualToString:@"Camera"]) {
            NSLog(@"%@", data);
            [self openCamera];
        } else if ([data isEqualToString:@"Picture"]) {
            NSLog(@"%@", data);
            [self openPhotoLibrary];
        }
        
        self.responseCallback = responseCallback;
    }];
    
    [self.bridge registerHandler:@"savePhoto" handler:^(id data, WVJBResponseCallback responseCallback) {
        if (data != nil) {
            NSLog(@"%@", data);
            NSData *decodedImageData = [[NSData alloc] initWithBase64EncodedString:data options:NSDataBase64DecodingIgnoreUnknownCharacters];
            UIImage *decodedImage = [UIImage imageWithData:decodedImageData];
            UIImageWriteToSavedPhotosAlbum(decodedImage, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
        }
        
        self.responseCallback = responseCallback;
    }];
    
    [self.bridge registerHandler:@"saveValue" handler:^(id data, WVJBResponseCallback responseCallback) {
        if (data != nil) {
            NSDictionary *dic = [self parseJSONStringToNSDictionary:data];
            
            NSUserDefaults *userDefalults = [NSUserDefaults standardUserDefaults];
            [userDefalults setObject:dic[@"value"] forKey:dic[@"key"]];
            [userDefalults synchronize];
        }
    }];
    
    [self.bridge registerHandler:@"getValue" handler:^(id data, WVJBResponseCallback responseCallback) {
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        NSString *str = [userDefaults objectForKey:data];
        if (str.length == 0) {
            responseCallback(@"");
        } else {
            responseCallback(str);
        }
    }];
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"index" ofType:@"html" inDirectory:@"dist"];
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"file://%@",filePath]]]];
    
//    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithUTF8String:"http://www.jisuhui7.com/"]]]];

}

- (NSDictionary*)parseJSONStringToNSDictionary:(NSString*)JSONString {
    NSData *JSONData = [JSONString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *responseJSON = [NSJSONSerialization JSONObjectWithData:JSONData options:NSJSONReadingMutableLeaves error:nil];
    return responseJSON;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if (self.isFirst == YES) {
        self.isFirst = NO;
        
        NSArray *arr;
        if (kISiPhone5 || kISiPhone6 || kISiPhone6P) {
            arr = @[@"bg_shanping"];
        } else if (kISiPhoneX || kISiPhoneXX || kISiPhoneXr) {
            arr = @[@"bg_shanpingxr"];
        }
        DHGuidePageHUD *guide = [[DHGuidePageHUD alloc] dh_initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight) imageNameArray:arr buttonIsHidden:YES];
        UIWindow *window = [[UIApplication sharedApplication].windows firstObject];
        [window addSubview:guide];
        
        [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(timerAction:) userInfo:guide repeats:NO];
    }
}

- (void)timerAction:(NSTimer *)timer {
    DHGuidePageHUD *alter=(DHGuidePageHUD *)[timer userInfo];
    [alter removeFromSuperview];
    alter = nil;
}

#pragma mark - 保存到相册
-(void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo {
    NSString *msg = nil ;
    if(error){
        msg = @"保存图片失败";
    }else{
        msg = @"保存图片成功";
    }
    self.responseCallback(msg);
    self.responseCallback = nil;
}

#pragma mark - 调用照相机
- (void)openCamera {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES; //可编辑

    //判断是否可以打开照相机
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        //摄像头
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:picker animated:YES completion:nil];

    } else {
        NSLog(@"没有摄像头");
    }
}

#pragma mark - 打开相册
- (void)openPhotoLibrary {
    // 进入相册
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc]init];

        imagePicker.allowsEditing = YES;
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        imagePicker.delegate = self;

        [self presentViewController:imagePicker animated:YES completion:^{
            NSLog(@"打开相册");
        }];
    } else {
        NSLog(@"不能打开相册");
    }
}

#pragma mark - UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *image = info[UIImagePickerControllerOriginalImage];

//    NSString *paths = NSTemporaryDirectory();
//    NSString *filePath = [paths stringByAppendingPathComponent:[NSString stringWithFormat:@"hexin%u.png",arc4random() % 9999]];
//
//    BOOL result = [UIImagePNGRepresentation(image) writeToFile:filePath atomically:YES]; // 保存成功会返回YES
//    if (result == YES) {
//        if (filePath && self.responseCallback) {
//            self.responseCallback(filePath);
//            self.responseCallback = nil;
//        }
//    }
    if (image != nil) {
        NSData *data = UIImagePNGRepresentation(image);
        NSString *encodedImageStr = [data base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
        self.responseCallback(encodedImageStr);
        self.responseCallback = nil;
    }

    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - 进入拍摄页面点击取消按钮
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - WKNavigationDelegate
#pragma mark - 页面开始加载时调用
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation {
    
}

#pragma mark - 当内容开始返回时调用
- (void)webView:(WKWebView *)webView didCommitNavigation:(WKNavigation *)navigation {
    
}

#pragma mark - 页面加载完成之后调用
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    
}

#pragma mark - 页面加载失败时调用
- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation withError:(NSError *)error {
    
}

#pragma mark - 接收到服务器跳转请求之后调用
- (void)webView:(WKWebView *)webView didReceiveServerRedirectForProvisionalNavigation:(WKNavigation *)navigation {
    
}

#pragma mark - 在收到响应后，决定是否跳转
- (void)webView:(WKWebView *)webView decidePolicyForNavigationResponse:(WKNavigationResponse *)navigationResponse decisionHandler:(void (^)(WKNavigationResponsePolicy))decisionHandler {
    decisionHandler(WKNavigationResponsePolicyAllow);
}

#pragma mark - 在发送请求之前，决定是否跳转
- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    NSURL *URL = navigationAction.request.URL;
    NSString *scheme = [URL scheme];
    if ([scheme isEqualToString:@"tel"]) {
        NSString *resourceSpecifier = [URL resourceSpecifier];
        NSString *callPhone = [NSString stringWithFormat:@"telprompt://%@", resourceSpecifier];
        /// 防止iOS 10及其之后，拨打电话系统弹出框延迟出现
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            dispatch_async(dispatch_get_main_queue(), ^{
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:callPhone]];
            });
        });
    }
    decisionHandler(WKNavigationActionPolicyAllow);
}

#pragma mark - get
- (WKWebView *)webView {
    if (!_webView) {
        _webView = [[WKWebView alloc] initWithFrame:self.view.bounds configuration:self.webViewConfig];
        _webView.backgroundColor = [UIColor whiteColor];
        _webView.navigationDelegate = self;
        //        _webView.allowsBackForwardNavigationGestures = YES;
        
        if ([[_webView.subviews firstObject] isKindOfClass:[UIScrollView class]]) {
            UIScrollView *scrollView = (UIScrollView *)[_webView.subviews firstObject];
            scrollView.bounces = NO;
        }
        if (@available(iOS 11.0, *)) {
            _webView.scrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        } else {
            self.automaticallyAdjustsScrollViewInsets = NO;
        }
        [self.view addSubview:_webView];
    }
    return _webView;
}

- (WKWebViewConfiguration *)webViewConfig {
    if (!_webViewConfig) {
        _webViewConfig = [[WKWebViewConfiguration alloc] init];
        _webViewConfig.allowsInlineMediaPlayback = YES;
        if (@available(iOS 9.0, *)) {
            _webViewConfig.allowsPictureInPictureMediaPlayback = YES;
        }
    }
    return _webViewConfig;
}


@end
