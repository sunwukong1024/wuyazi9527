//
//  TXHUserHelper.h
//  CarHouseRemit
//
//  Created by Dreamer on 2019/4/1.
//  Copyright © 2019 Dreamer. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TXHUserHelper : NSObject

+ (BOOL)isFirstOpen;

@end

NS_ASSUME_NONNULL_END
